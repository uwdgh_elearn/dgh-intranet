<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" lang="en" xml:lang="en" class="no-js">
    <head>
        <title> <?php wp_title(' | ',TRUE,'right'); bloginfo('name'); ?> </title>
        <meta charset="utf-8">
        <meta name="description" content="<?php bloginfo('description', 'display'); ?>">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <!-- Place favicon.ico and apple-touch-icon.png in the root directory -->

        <?php wp_head(); ?>

        <!--[if lt IE 9]>
            <script src="<?php bloginfo("template_directory"); ?>/assets/ie/js/html5shiv.js" type="text/javascript"></script>
            <script src="<?php bloginfo("template_directory"); ?>/assets/ie/js/respond.js" type="text/javascript"></script>
            <link rel='stylesheet' href='<?php bloginfo("template_directory"); ?>/assets/ie/css/ie.css' type='text/css' media='all' />
        <![endif]-->

        <?php
        echo get_post_meta( get_the_ID() , 'javascript' , 'true' );
        echo get_post_meta( get_the_ID() , 'css' , 'true' );
        ?>
        <!--//noscript message//--><noscript><!--//noscript message//--><div style="font-size: 12px; background-color: #fef5f1; border: 1px solid #ed541d; padding: 10px;"><samp>This site uses JavaScript to accomplish certain tasks and provide additional functionality in your browser. If you are seeing this message, then something has prevented JavaScript from running and may cause issues when using the site. Please enable JavaScript on your web browser.</samp></div></noscript>
    </head>
    <!--[if lt IE 9]> <body <?php body_class('lt-ie9'); ?>> <![endif]-->
    <!--[if gt IE 8]><!-->
    <body <?php body_class(); ?> >
    <!--<![endif]-->

    <div id="uwsearcharea" aria-hidden="true" class="uw-search-bar-container"></div>
    <div id="uwdghsearcharea" aria-hidden="true" class="uw-search-bar-container" hidden>
      <?php
        $locations = get_theme_mod('nav_menu_locations');
        if ( $locations['uwdghsearcharea-menu'] ) {
          /* Display the thinstrip menu location */
          wp_nav_menu(array(
            'theme_location' => 'uwdghsearcharea-menu',
            'menu_class' => 'uwdghsearcharea-menu',
            'container' => 'div',
            'container_class' => 'uwdghsearcharea-menu-wrapper container',
          ));
        }
      ?>
    </div>

    <a id="main-content" href="#main_content" class="screen-reader-shortcut">Skip to main content</a>

    <div id="uw-container">

    <div id="uw-container-inner">

    <?php if (get_option('uwdgh_theme_show_watermark')) : ?>
    <input type="hidden" name="uwdgh_theme_show_watermark_text" value="<?php echo get_option('uwdgh_theme_show_watermark_text'); ?>">
    <?php endif; ?>

    <?php get_template_part('thinstrip'); ?>

    <?php require( get_template_directory() . '/inc/template-functions.php' );
          uw_dropdowns(); ?>
