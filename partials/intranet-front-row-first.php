<?php
$shortcode = <<<EOD
[su_row]
[su_column size="1/3"]
<h2 style="display:inline-block;">Announcements</h2><div class="rss"><a href="category/announcement/feed" title="Announcements RSS feed" class="rss-link">rss</a></div>
[su_posts template="partials/teaser-details-loop-uwdgh.php" taxonomy="category" tax_term="announcement" posts_per_page=5]
<a href="category/announcement/" class="more" title="Announcements">More Announcements</a>
[/su_column]
[su_column size="1/3"]
<h2 style="display:inline-block;">Opportunities</h2><div class="rss"><a href="category/opportunity/feed" title="Opportunities RSS feed" class="rss-link">rss</a></div>
[su_posts template="partials/teaser-details-loop-uwdgh.php" taxonomy="category" tax_term="opportunity" posts_per_page=15]
<!--details>
<summary class="uw-teaser-view-more">View more</summary>
[su_posts template="partials/teaser-details-loop-uwdgh.php" taxonomy="category" tax_term="opportunity" posts_per_page=7 offset=5]
</details-->
<a href="category/opportunity/" class="more" title="Opportunities">More Opportunities</a>
[/su_column]
[su_column size="1/3"]
[dgh_scheduled_meetings show="5" title="Upcoming meetings" category=""]
<!--a href="category/meeting/" class="more">More Meetings</a-->
[/su_column]
[/su_row]
EOD;
echo do_shortcode( $shortcode );
