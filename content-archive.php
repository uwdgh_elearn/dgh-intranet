<?php
// get the parent category slug
$category = get_the_category();
$category_parent_id = $category[0]->category_parent;
if ( $category_parent_id != 0 ) {
    $category_parent = get_term( $category_parent_id, 'category' );
    $category_parent_slug = $category_parent->slug;
} else {
    $category_parent_slug = $category[0]->slug;
}

// get meeting date field
$meeting_date = get_post_meta($post->ID, 'meeting_date', true);

?>
<h3 style="color:#4b2e83;">
  <a href="<?php the_permalink() ?>" title="<?php the_title_attribute(); ?>"><?php the_title() ?></a>
</h3>
<?php if ( $category_parent_slug != 'meeting' ) : ?>
  <?php //the_date('F j, Y', '<p class="date">', '</p>'); ?>
  <p><span class="uw-teaser-link-date"><?php _e( 'Posted', 'uwdgh' ); ?>: <time datetime="<?php the_time(DATE_ATOM); ?>"><?php the_time( get_option( 'date_format' ) ); ?></time></span></p>
<?php endif; ?>
<?php
 // display meeting date
if( ($category_parent_slug == 'meeting') && ($meeting_date) ) {
  $meeting_date = date_create($meeting_date);
  echo '<h6>Meeting date: <time datetime="' . date_format($meeting_date, DATE_ATOM) . '">' . date_format($meeting_date, get_option( 'date_format' )) . '</time></h6>';
}
?>
<?php
if (get_option('show_byline_on_posts')) :
?>
<div class="author-info">
    <?php the_author(); ?>
    <p class="author-desc"> <small><?php the_author_meta(); ?></small></p>
</div>
<?php
endif;
  if ( ! is_home() && ! is_search() && ! is_archive() ) :
    uw_mobile_menu();
  endif;
 if ( has_post_thumbnail() ) :
 	the_post_thumbnail( 'thumbnail' , 'style=margin-bottom:5px;');
 endif;
?>
<?php the_excerpt(); ?>
<?php get_template_part('uwdgh-tags'); ?>
<hr>
