<?php defined( 'ABSPATH' ) || exit; ?>

<?php
/**
 * READ BEFORE EDITING!
 *
 * Do not edit templates in the plugin folder, since all your changes will be
 * lost after the plugin update. Read the following article to learn how to
 * change this template or create a custom one:
 *
 * https://getshortcodes.com/docs/posts/#built-in-templates
 */
?>
<div class="su-posts su-posts-teaser-loop su-posts-teaser-loop__uw">
	<?php if ( $posts->have_posts() ) : ?>
		<?php while ( $posts->have_posts() ) : ?>
			<?php $posts->the_post(); ?>

			<div id="su-post-<?php the_ID(); ?>" class="su-post">
				<?php if ( has_post_thumbnail() ) : ?>
					<a class="su-post-thumbnail" href="<?php the_permalink(); ?>"><?php the_post_thumbnail(); ?></a>
				<?php endif; ?>
				<a class="uw-teaser-link" href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
				<span class="uw-teaser-link-date"><?php _e( 'Posted', 'shortcodes-ultimate' ); ?>: <time datetime="<?php the_time(DATE_ATOM); ?>"><?php the_time( get_option( 'date_format' ) ); ?></time></span>
			</div>

		<?php endwhile; ?>
	<?php else : ?>
		<p class="su-posts-not-found"><?php esc_html_e( 'Posts not found', 'shortcodes-ultimate' ); ?></p>
	<?php endif; ?>
</div>
