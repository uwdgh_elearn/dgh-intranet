<div class="container container--uwdgh-notice">
  <div class="row">
    <style>
      .container--uwdgh-notice {
        border-top: 1px solid #d1d1d1;
        padding: 1em;
      }
      .container--uwdgh-notice .notice {
        display: inline-block;
        background: #fff;
        border: 1px solid #c3c4c7;
        border-left-width: 4px;
        box-shadow: 0 1px 1px rgb(0 0 0 / 4%);
        margin: 5px 0 15px;
        padding: 1px 12px;
        font-size: 12px;
      }
      .container--uwdgh-notice .notice-info {
          border-left-color: #72aee6;
      }
      .container--uwdgh-notice .notice-info::before{
        content: '\02139';
        width: 10px;
        color: #ffffff;
        background-color: #72aee6;
        position: relative;
        line-height: 0;
        font-size: 18px;
        padding: 0 1rem;
        top: 3px;
        left: -16px;
        margin-right: -10px;
      }
      .container--uwdgh-notice .notice-success{
          border-left-color: #00a32a;
      }
      .container--uwdgh-notice .notice-warning {
          border-left-color: #dba617;
      }
      .container--uwdgh-notice .notice-error {
          border-left-color: #d63638;
    }
    </style>
    <?php
    $today = date_create("now", timezone_open(get_option('timezone_string')));
    $moddate = date_create(get_the_modified_date(), timezone_open(get_option('timezone_string')));
    $interval = date_diff($today, $moddate);
    // echo $interval->format('%R%a days');
    // echo '<pre>';
    // var_dump($today);
    // echo '</pre>';
    // echo '<pre>';
    // var_dump($moddate);
    // echo '</pre>';
    // echo '<pre>';
    // var_dump($interval);
    // echo '</pre>';

    // the $status variable is currently not used
    // all notices show the info style
    // if ($interval->days >= 365){
    //   $status = 'error';
    // } elseif ($interval->days >= 100 && $interval->days < 365) {
    //   $status = 'warning';
    // } elseif ($interval->days >= 30 && $interval->days < 100) {
    //   $status = 'info';
    // } else {
    //   $status = 'success';
    // }

    ?>
    <div class="notice notice-info">
      This page was last updated <?php if ($interval->days == 0) { _e('today'); }
      elseif ($interval->days == 1) { _e('yesterday'); }
      else { echo $interval->format('%a days ago'); } ?>: <date datetime="<?php echo get_the_modified_date('Y-m-d H:i:s'); ?>"><?php the_modified_date(get_option('date_format')); ?></date>
    </div>
  </div>
</div>
