<?php
/* Add theme styles and scripts */
function uwdgh_intranet_scripts_and_styles() {

    wp_register_script('dgh-intranet', get_stylesheet_directory_uri() . '/js/dgh-intranet.js', array('jquery'));
    wp_enqueue_script('dgh-intranet');
}
add_action( 'wp_enqueue_scripts', 'uwdgh_intranet_scripts_and_styles');

/* register uwdghsearcharea menu location */
function uwdgh_register_uwdghsearcharea_menu() {
    register_nav_menu('uwdghsearcharea-menu',__( 'UWDGH Searcharea','uwdgh' ));
}
add_action( 'init', 'uwdgh_register_uwdghsearcharea_menu' );

/**
* Shorcode function for retrieving Posts under the meeting category and
* using custom field meeting_date
* shortcode use example: [dgh_scheduled_meetings category="meeting" show="3" title="Schedule meetings"]
* attributes:
* * category [optional], default value is 'meeting'
* * show [optional], default value is -1 (show all)
* * title [optional], default value is 'Scheduled meetings'
*/
function uwdgh_scheduled_meetings($atts = []) {
    // The query to fetch future posts
    $args = array(
        'post_type' => 'post',
        'posts_per_page' => -1,
        'meta_key' => 'meeting_date',
        'meta_type' => 'DATE',
        'orderby' => 'meta_value',
        'order' => 'ASC'
    );
    //attribute handling
    $teaser_title = 'Scheduled meetings'; // default teaser title
    $show = -1;  // instantiate show all
    if ($atts) {
      // category
      $args['category_name'] = $atts['category'];
      // number of posts to show
      if ( is_numeric( $atts['show'] ) ) { $show = $atts['show']; }
      // Teaser title
      if ( $atts['title'] ) { $teaser_title = sanitize_text_field( $atts['title'] ); }
    } else {
      // get all meeting categories
      $meeting_cat = get_categories(array('slug'=>'meeting'));
      $args['cat'] = $meeting_cat[0]->term_id;
    }

    $the_query = new WP_Query( $args );

    $output = '<h2>' . $teaser_title . '</h2>';
    // The loop to display posts
    if ( $the_query->have_posts() ) {
      $i = 0; // counter for future dates
      $output .= '<div class="uwdgh-meeting-teaser-loop">';
      while ( $the_query->have_posts() ) {
        if ($i == $show)
          break;
        $the_query->the_post();

        $meeting_date = get_post_meta( get_the_ID(), 'meeting_date', true);
        if ( $meeting_date ) {
          $meeting_date = date_create($meeting_date);
          $today = date_create("now");
          if ( date_format($meeting_date, "Y-m-d") >= date_format($today, "Y-m-d") ) {
            $output .= '<div id="uwdgh-post-' . get_the_ID() . '" class="uwdgh-meeting-post">';
            $output .= '<div><small><time datetime="' . date_format($meeting_date, "Y-m-d") . '">' . (( date_format($meeting_date, "Y-m-d") === date_format($today, "Y-m-d") ) ? 'Today, ' : '' ) . date_format($meeting_date, "F j, Y") . '</time></small></div>';
            $output .= '<details><summary class="uw-teaser-link" title="' . esc_attr( get_the_title() ) . '">' . get_the_title() . '</summary>';
            $output .= '<div class="uw-teaser-details-content uw-teaser-details-content--white"><p>' . get_the_excerpt() . '</p>';
            $output .= '<a class="more" href="' . get_the_permalink() . '" title="' . esc_attr( get_the_title() ) . '">Read more</a>';
            $output .= '</div></details>';
            $output .= '</div>';
            $i++;
          }
        }
      }
      $output .= '</div>';
      if ($i==0) {
				$output = '<h2>' . $teaser_title . '</h2>';
        $output .= '<p>No meetings scheduled.</p>';
      }
    } else {
        // Show this when no future posts are found
				$output = '<h2>' . $teaser_title . '</h2>';
        $output .= '<p>No Posts available.</p>';
    }
    // $_more = ($args['category_name']) ? '/' . $args['category_name'] . '/' : '/';
    // $output .= '<a href="category/meeting' . $_more  . '" class="more">More</a>';
    // Reset post data
    wp_reset_postdata();

    // Return output
    return $output;
}
// Add shortcode
add_shortcode('dgh_scheduled_meetings', 'uwdgh_scheduled_meetings');
// Enable shortcode execution inside text widgets
add_filter('widget_text', 'do_shortcode');


/*** START Attachments ***/
// requires: https://wordpress.org/plugins/attachments/
// docs: https://github.com/jchristopher/attachments/blob/master/docs/usage.md

/**
* Disable the Default Attachments Instance
*/
add_filter( 'attachments_default_instance', '__return_false' ); // disable the default instance

/**
* Create new Attachments Instance
*/
function uwdgh_attachments( $attachments ) {
  $fields         = array(
    array(
      'name'      => 'title',                         // unique field name
      'type'      => 'text',                          // registered field type
      'label'     => __( 'Title', 'attachments' ),    // label to display
      'default'   => 'title',                         // default value upon selection
    ),
    array(
      'name'      => 'caption',                       // unique field name
      'type'      => 'textarea',                      // registered field type
      'label'     => __( 'Caption', 'attachments' ),  // label to display
      'default'   => 'caption',                       // default value upon selection
    ),
  );

  $args = array(
    // title of the meta box (string)
    'label'         => 'Attachments',
    // all post types to utilize (string|array)
    'post_type'     => array( 'post', 'page' ),
    // meta box position (string) (normal, side or advanced)
    'position'      => 'normal',
    // meta box priority (string) (high, default, low, core)
    'priority'      => 'high',
    // allowed file type(s) (array) (image|video|text|audio|application)
    'filetype'      => null,  // no filetype limit
    // include a note within the meta box (string)
    'note'          => 'Attach files here!',
    // by default new Attachments will be appended to the list
    // but you can have then prepend if you set this to false
    'append'        => true,
    // text for 'Attach' button in meta box (string)
    'button_text'   => __( 'Attach Files', 'attachments' ),
    // text for modal 'Attach' button (string)
    'modal_text'    => __( 'Attach', 'attachments' ),
    // which tab should be the default in the modal (string) (browse|upload)
    'router'        => 'browse',
    // whether Attachments should set 'Uploaded to' (if not already set)
    'post_parent'   => false,
    // fields array
    'fields'        => $fields,
  );

  $attachments->register( 'uwdgh_attachments', $args ); // unique instance name
}
add_action( 'attachments_register', 'uwdgh_attachments' );

/**
* Attachments output
*/
function uwdgh_get_attachments () {
  // check for plugin
  if ( is_plugin_active('attachments/index.php') == false ) {
    return;
  }
  ?>
  <?php $attachments = new Attachments( 'uwdgh_attachments' ); /* pass the instance name */ ?>
  <?php if( $attachments->exist() ) : ?>
    <style>
      .uwdgh-attachments {}
      .uwdgh-attachments-label {
        position: relative;
        padding-bottom: 20px;
        clear: left;
      }
      .uwdgh-attachments-label::before,
      .uwdgh-attachments-label::after  {
        position: absolute;
        left: 0;
        bottom: 5px;
        content: "";
        height: 4px;
      }
      .uwdgh-attachments-label::before {
        width: 100px;
        background-color: #b7a57a;
      }
      .uwdgh-attachments-label::after {
        width: 40px;
        -webkit-transform: skewX(-25deg) skewY(0);
        -o-transform: skewX(-25deg) skewY(0);
        transform: skewX(-25deg) skewY(0);
        -webkit-transform: skew(-25deg, 0);
        -ms-transform: skewX(-25deg) skewY(0);
        transform: skew(-25deg, 0);
        left: 80px;
        background-color: white;
        bottom: 3px;
        height: 8px;
      }
      .uwdgh-attachments-label data:hover::after,
      .uwdgh-attachments-label data:focus::after {
        content: '\000A0\0279D\000A0' attr(value) ' <?php _e('attachment(s)', 'uwdgh') ?>';
        font-family: "Open Sans", sans-serif;
        color: #c9c9c9;
        font-size: .7em;
        font-weight: normal;
      }
      .uwdgh-attachments details {
        font-style: italic;
      }
      a.uw-btn.uw-btn--attachment {
        text-transform: none;
        width: 100%;
        margin-bottom: 0;
        margin-right: -50px;
      }
      a.uw-btn.uw-btn--attachment::before,
      a.uw-btn.uw-btn--attachment::after {
        content: none;
      }
      .uw-btn--attachment-thumbnail {
        float: right;
        margin-left: .5em;
      }
      .uw-btn--attachment-thumbnail img {
        max-width: 32px;
      }
      .uw-btn--attachment-title {
        display: initial;
      }
      .uw-btn--attachment-filesize,
      .uw-btn--attachment-fileextension {
        float: right;
        margin-left: .5em;
        font-style: italic;
        font-weight: normal;
        font-size: 80%;
      }
      .uw-btn--attachment-caption {
        margin-bottom: -.5em;
        font-size: 90%;
        font-weight: normal;
        padding-right: 50px;
      }
      .uw-btn-attachment-download {
        text-align: right;
        font-size: 75%;
        margin-bottom: 1em;
      }
    </style>
    <section class="uwdgh-attachments">
      <h2 class="uwdgh-attachments-label"><data value="<?php echo $attachments->total(); ?>"><?php _e('Attachments', 'uwdgh') ?></data></h2>
      <details style="margin-bottom: 1em;">
        <summary><?php _e('Details', 'uwdgh') ?></summary>
        <?php _e('Attachments will open in your browser or download to your device, depending on the type of file, browser and device. Click the "Download Attachment" link to download the attached file.', 'uwdgh') ?>
      </details>
      <?php while( $attachments->get() ) : ?>
        <?php
        $_mime_type = $attachments->type() . '/' . $attachments->subtype();
        $_file_extension = uwdgh_get_file_extension_by_mime_type($_mime_type);
        ?>
        <a class="uw-btn uw-btn--attachment" href="<?php echo $attachments->url(); ?>" title="<?php echo $attachments->field( 'title' ); ?>">
          <div class="uw-btn--attachment-thumbnail" title="File type: <?php echo $_mime_type; ?>"><?php echo $attachments->image( 'thumbnail' ); ?></div>
          <div class="uw-btn--attachment-filesize" title="File size: <?php echo $attachments->filesize(); ?>"><?php echo $attachments->filesize(); ?></div>
          <div class="uw-btn--attachment-fileextension" title="File extension: <?php echo $_file_extension; ?>"><?php echo $_file_extension; ?></div>
          <div class="uw-btn--attachment-title"><?php echo $attachments->field( 'title' ); ?></div>
          <?php if ($attachments->field( 'caption' )) : ?>
            <div class="uw-btn--attachment-caption"><?php echo $attachments->field( 'caption' ); ?></div>
          <?php endif; ?>
        </a>
        <div class="uw-btn-attachment-download"><a download href="<?php echo $attachments->url(); ?>" title="Download '<?php echo $attachments->field( 'title' ); ?>'"><?php _e('Download Attachment', 'uwdgh') ?></a></div>
      <?php endwhile; ?>
    </section>
  <?php endif; ?>
  <?php
}
/*** END Attachments ***/

/**
* Get file extension by mime type
* e.g.: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet' returns 'xlsx'
* or FALSE otherwise
*/
function uwdgh_get_file_extension_by_mime_type( $mime_type ) {
  $allowed_mime_values = get_allowed_mime_types();
  return array_search($mime_type, $allowed_mime_values);
}

/* get UW styled taxonomy terms via shortcode
*  attributes:
* * taxonomy [optional], default value is 'category'
* e.g.: [uwdgh_tax_terms taxonomy="post_tag"]
*/
function uwdgh_tax_terms( $atts = [] ) {
  $taxonomy = get_taxonomy( 'category' );
  if ( $atts['taxonomy'] ) {
    $taxonomy = get_taxonomy( $atts['taxonomy'] );
  }
  if ($taxonomy) {
    $output = '<h2>' . $taxonomy->label . '</h2>';
    $terms = get_terms( $taxonomy->name );
    foreach ( $terms as $term ) {
      $output .='<span class="uw-tag"><a rel="tag" href="' . get_term_link($term->slug, $taxonomy->name) . '">' . $term->name . '</a></span> ';
    }
  } else {
    $output = '<div style="color: red; font-weight: bold;">Taxonomy <i>' . $atts['taxonomy'] . '</i> not found!</div>';
  }
  return $output;
}
// Add shortcode
add_shortcode('uwdgh_tax_terms', 'uwdgh_tax_terms');
// Enable shortcode execution inside text widgets
add_filter('widget_text', 'do_shortcode');
