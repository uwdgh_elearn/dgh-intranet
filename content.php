<?php
// get the parent category slug
$category = get_the_category();
$category_parent_id = $category[0]->category_parent;
if ( $category_parent_id != 0 ) {
    $category_parent = get_term( $category_parent_id, 'category' );
    $category_parent_slug = $category_parent->slug;
} else {
    $category_parent_slug = $category[0]->slug;
}

if ((is_single() || is_home()) && ($category_parent_slug != 'meeting')){
    the_date('F j, Y', '<p class="date"><small>', '</small></p>');
}
?>
<h1><?php the_title() ?></h1>
<?php
if ((is_single() || is_home()) && get_option('show_byline_on_posts')) :
?>
<div class="author-info">
    <?php if ( function_exists( 'coauthors' ) ) { coauthors(); } else { the_author(); } ?>
    <p class="author-desc"> <small><?php the_author_meta(); ?></small></p>
</div>
<?php
endif;
  if ( ! is_home() && ! is_search() && ! is_archive() ) :
    uw_mobile_menu();
  endif;

?>

<?php


  if ( is_archive() || is_home() ) {
    the_post_thumbnail( array(130, 130), array( 'class' => 'attachment-post-thumbnail blogroll-img' ) );
    the_excerpt();
    echo "<hr>";
  } else {
    if ($category_parent_slug = 'meeting') {
      // echo '<var>';
      // print_r(get_post_meta($post->ID, 'meeting_date', false));
      // print_r(pods_field('post', $post->ID, 'meeting_date', false));
      // echo '</var>';
      // meeting date
      $meeting_date = get_post_meta($post->ID, 'meeting_date', true);
      if($meeting_date) {
        $meeting_date = date_create($meeting_date);
        echo '<h6>Meeting date: ' . date_format($meeting_date, "F j, Y") . '</h6>';
      }
    }

    // check if the post or page has a Featured Image assigned to it.
    if ( has_post_thumbnail() ) {
      $large_image_url = wp_get_attachment_image_src( get_post_thumbnail_id(), 'large');
      echo '<a href="' . $large_image_url[0] . '" title="' . the_title_attribute('echo=0') . '" >';
      the_post_thumbnail();
      echo '</a>';
    }
    the_content();
    uwdgh_get_attachments();
    //comments_template(true);
  }
 ?>
 <hr>
 <?php get_template_part('uwdgh-tags'); ?>
