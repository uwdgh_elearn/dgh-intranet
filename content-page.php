

<?php if ( (is_home() || is_front_page()) && get_option('uwdgh_theme_hide_front_page_title') ) { ?>
  <!--//hide front page title//-->
<?php } else { ?>
  <h1><?php the_title(); ?></h1>
<?php } ?>

<?php if(uw_list_pages()){ ?>
	<div id="mobile-sidebar">
		<button id="mobile-sidebar-menu" class="visible-xs" aria-hidden="true" tabindex="1">
	    	<div aria-hidden="true" id="ham">
		    	<span></span>
				<span></span>
				<span></span>
				<span></span>
			</div>
	   		<div id="mobile-sidebar-title" class="page_item"> Close Menu </div>
		</button>
		<div id="mobile-sidebar-links" aria-hidden="true" class="visible-xs">  <?php uw_sidebar_menu(); ?></div>
	</div>
<?php } ?>

<?php the_content(); ?>
<?php uwdgh_get_attachments(); ?>
