(function ($, window, document) {


  // The document ready event executes when the HTML-Document is loaded
  // and the DOM is ready.
  jQuery(document).ready(function( $ ) {

    // View more toggle
    $(document).on('touchstart', 'summary.uw-teaser-view-more', function(){
      touchstart = true;
    });
    $(document).on('touchmove', 'summary.uw-teaser-view-more', function(){
      touchmove = true;
    });
    $(document).on('touchend click', 'summary.uw-teaser-view-more', function(e){
      if ( ((e.type == "click")) || (touchstart && !touchmove) ){
        toggleViewmore($(this));
      }
    });
    function toggleViewmore (el) {
      if ( $(el).text().toLowerCase() == 'view more' ) {
        $(el).text('View less');
      } else {
        $(el).text('View more');
      }
    }

  })//document.ready

  // The window load event executes after the document ready event,
  // when the complete page is fully loaded.
  jQuery(window).load(function () {

    // remove the uw-2014 theme's search area from the DOM
    $('#uwsearcharea').remove();
    // on-click event for the uw-search button
    $('button.uw-search').on('click', function(e) {
      if ( $(this).attr('aria-expanded')=='true' ) {
        $('#uwdghsearcharea')
          .attr('role', 'search')
          .attr('aria-hidden', false)
          .prop('hidden', false);
      } else {
        $('#uwdghsearcharea')
          .removeAttr('role')
          .attr('aria-hidden', true)
          .prop('hidden', true);
      }
    });

  })//window.load


})(jQuery, this, this.document);
